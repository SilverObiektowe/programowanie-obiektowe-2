﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Programowanie_Obiektowe_2_Silver.NET;
using System.Reflection;
using System.Collections.Generic;

namespace Programowanie_Obiektowe_2_Testy
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void A_Klasa_Firma_Posiada_Pole_Nazwa()
        {
            bool passed = Helper.KlasaFirmaPosiadaPoleONazwie("nazwa", typeof(string), true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void A_Klasa_Firma_Posiada_Pole_nip()
        {
            bool passed = Helper.KlasaFirmaPosiadaPoleONazwie("nip", typeof(string), true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void A_Klasa_Firma_Posiada_Wlasciwosc_NIP()
        {
            bool passed = Helper.KlasaFirmaPosiadaPubliczneWlasciwosciONazwie("NIP", typeof(string), true, false);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void A_Klasa_Firma_Posiada_Wlasciwosc_Nazwa()
        {
            bool passed = Helper.KlasaFirmaPosiadaPubliczneWlasciwosciONazwie("Nazwa", typeof(string), true, true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void A_Klasa_Firma_Posiada_Dwuparametrowy_Konstruktor()
        {
            ConstructorInfo[] constructors = typeof(Firma).GetConstructors();

            foreach (ConstructorInfo constructor in constructors)
            {
                if (constructor.GetParameters().Length == 2 || 
                    constructor.GetParameters().Length == 3)
                {
                    Assert.IsTrue(true);
                    return;
                }
            }

            Assert.Fail();
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Pole_imie()
        {
            bool passed = Helper.KlasaPracownikPosiadaPoleONazwie("Imie" , typeof(string), true);
            Assert.IsTrue(passed);
        }
        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Pole_nazwisko()
        {
            bool passed = Helper.KlasaPracownikPosiadaPoleONazwie("Nazwisko", typeof(string), true);
            Assert.IsTrue(passed);
        }
        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Pole_wiek()
        {
            bool passed = Helper.KlasaPracownikPosiadaPoleONazwie("Wiek", typeof(int), true);
            Assert.IsTrue(passed);
        }
        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Pole_placa()
        {
            bool passed = Helper.KlasaPracownikPosiadaPoleONazwie("Placa", typeof(double), true);
            Assert.IsTrue(passed);
        }
        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Pole_stanowisko()
        {
            bool passed = Helper.KlasaPracownikPosiadaPoleONazwie("Stanowisko", typeof(string), true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Firma_Posiada_Wlasciwosc_Nazwa()
        {
            bool passed = Helper.KlasaFirmaPosiadaPubliczneWlasciwosciONazwie("Nazwa", typeof(string), true, true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_Imie()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie("Imie", typeof(string), true, true);
            Assert.IsTrue(passed);
        }
        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_Nazwisko()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("Nazwisko", typeof(string), true, true);
            Assert.IsTrue(passed);
        }
        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_Wiek()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("Wiek", typeof(int), true, true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_Placa()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("Placa", typeof(double), true, true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_Stanowisko()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("Stanowisko", typeof(string), true, true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_ImieINazwisko()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("ImieINazwisko", typeof(string), true, false);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_StanowiskoPracownika()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("StanowiskoPracownika", typeof(string), true, false);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Wlasciwosc_WiekStanowisko()
        {
            bool passed = Helper.KlasaPracownikPosiadaPubliczneWlasciwosciONazwie
                ("WiekStanowisko", typeof(string), true, false);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void B_Klasa_Pracownik_Posiada_Piecioparametrowy_Konstruktor()
        {
            ConstructorInfo[] constructors = typeof(Pracownik).GetConstructors();

            foreach (ConstructorInfo constructor in constructors)
            {
                if (constructor.GetParameters().Length >= 5)
                {
                    Assert.IsTrue(true);
                    return;
                }
            }

            Assert.Fail();
        }

        [TestMethod]
        public void C_Klasa_Firma_Posiada_Pole_listaPracowników()
        {
            bool passed = Helper.KlasaFirmaPosiadaPoleONazwie("listaPracownikow", typeof(List<Pracownik>), true);
            Assert.IsTrue(passed);
        }

        [TestMethod]
        public void C_Klasa_Firma_Posiada_Zmieniony_Konstriktor()
        {
            ConstructorInfo[] constructors = typeof(Firma).GetConstructors();

            foreach (ConstructorInfo constructor in constructors)
            {
                if (constructor.GetParameters().Length > 2)
                {
                    Assert.IsTrue(true);
                    return;
                }
            }

            Assert.Fail();
        }
    }

    public static class Helper
    {
        public static bool KlasaFirmaPosiadaPoleONazwie(string nazwa, Type typPola, bool prywatne = true)
        {
            FieldInfo[] fields = typeof(Firma).GetFields(
                         BindingFlags.NonPublic |
                         BindingFlags.Instance);

            foreach (FieldInfo field in fields)
            {
                if (field.Name.ToLower() == nazwa.ToLower() &&
                    field.FieldType == typPola &&
                    field.IsPrivate == prywatne)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool KlasaFirmaPosiadaPubliczneWlasciwosciONazwie(string nazwa, Type typWlasciwosci,
            bool doOdczytu, bool doZapisu)
        {
            PropertyInfo[] properties = typeof(Firma).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo property in properties)
            {
                if (property.Name.ToLower() == nazwa.ToLower() &&
                    property.PropertyType == typWlasciwosci &&
                    property.CanRead == doOdczytu &&
                    property.CanWrite == doZapisu)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool KlasaPracownikPosiadaPoleONazwie(string nazwa, Type typPola, bool prywatne = true)
        {
            FieldInfo[] fields = typeof(Pracownik).GetFields(
                         BindingFlags.NonPublic |
                         BindingFlags.Instance);

            foreach (FieldInfo field in fields)
            {
                if (field.Name.ToLower() == nazwa.ToLower() &&
                    field.FieldType == typPola &&
                    field.IsPrivate == prywatne)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool KlasaPracownikPosiadaPubliczneWlasciwosciONazwie(string nazwa, Type typWlasciwosci,
            bool doOdczytu, bool doZapisu)
        {
            PropertyInfo[] properties = typeof(Pracownik).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo property in properties)
            {
                if (property.Name.ToLower() == nazwa.ToLower() &&
                    property.PropertyType == typWlasciwosci &&
                    property.CanRead == doOdczytu &&
                    property.CanWrite == doZapisu)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
